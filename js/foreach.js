(function ($) {
    var def = {
        animateSpeed: 400
    },
    methods = {
        init: function (elem) {
          return $('<div class="fe-select-container fe-'+ elem.attr('id') +'"></div>')
              .append('<div class="fe-selected"><input disabled class="fe-selected-value" type="text"><span class="fe-caret"></span></div>')
              .append('<ul class="fe-selection-list"></ul>')
        },
        build: function (elem, container) {
            elem.find('option').each(function () {
                    if($(this).attr('selected')){
                        $(container).find('.fe-selection-list')
                            .append('<li class="selected">'+ $(this).text() +'</li>');
                    } else {
                        $(container).find('.fe-selection-list')
                            .append('<li>'+ $(this).text() +'</li>');
                    }
            })
        },
        post: function (container, elem) {
            $(container).find('.fe-selected-value')
                .val(elem.text());
            elem.addClass('selected')
        },
        logic: function (elem, container, settings) {
            elem.find('option')
                .each(function () {
                    if($(this).attr('selected')){
                        methods.post(container, $(this));
                    }
                });
            if(!$(container).find('li').is('.selected')){
                methods.post(container, $(container).find('.fe-selection-list').find('li:first'));
            }
            $(container).find('li').each(function () {
                $(this).click(function () {
                    $(this).siblings().removeClass('selected');
                    methods.post(container, $(this));
                    methods.close(container, settings);
                })
            });
            elem.remove();
        },
        open: function (container, settings) {
            var height = $(container).find('.fe-selection-list').height();
            $(container).find('.fe-selection-list').css('height', '0')
                .hide();
            $(container).find('.fe-selected').click(function () {
                if($(container).hasClass('opened')){
                    methods.close(container, settings);
                } else {
                    $(container).addClass('opened');
                    $(this).next().show().stop().animate({
                        height: height
                    }, settings.animateSpeed)
                    methods.mouseout(container, settings);
                }
            });
        },
        close: function (container, settings) {
            $(container).find('.fe-selection-list')
                .stop()
                .animate({
                    height: 0
                }, settings.animateSpeed);
                setTimeout(function (settings) {
                    $(container).removeClass('opened').find('.fe-selection-list')
                        .hide();
                }, settings.animateSpeed + 10);
            $(document).unbind();
        },
        mouseout: function (container, settings) {
            $(document).mouseup(function (e) {
                var element = $(container);
                if (element.has(e.target).length === 0){
                    methods.close(container, settings);
                }
            });
        }
    };
    $.fn.feSelect = function (options) {
        return this.each(function () {
            var self = $(this),
                self_container = '.fe-' + self.attr('id');
            var settings = $.extend({}, def, options);
            if(self.is('select') && self.attr('id')){
                self.before(methods.init(self));
                methods.build(self, self_container);
                methods.logic(self, self_container, settings);
                methods.open(self_container, settings);
            } else {
                console.warn(self.selector + ' Is not select or don\'t have id');
            }
        });
    };
})(jQuery);


(function ($) {
    var def = {

        },
        methods = {
            init: function (elem) {
                /*$('<div class="fe-paralax"></div>',{
                    style:'width: 100%;height: auto;position: absolute;top:0;left:0;' +
                    'background-image:' + elem.data('image')
                }).wrapInner(elem);*/
                var height = elem.height();
                elem.wrapInner('<div class="fe-paralax"></div>').css({
                    position: 'relative',
                    overflow: 'hidden',
                    height: height + 'px'
                }).find('.fe-paralax').css({
                    width: '100%',
                    position: 'absolute',
                    left: '0',
                    top: '0',
                    backgroundImage: 'url(' + elem.data('image') + ')',
                    backgroundSize: '100%',
                    height: height + 'px'
                });
                methods.verification(elem);
            },
            verification: function (elem) {
                $(window).scroll(function () {
                    if((elem.offset().top <= $(this).scrollTop() + $(this).height())){
                        methods.effect(elem);
                    }
                })
            },
            effect: function (elem) {
                $(window).scroll(function () {
                    elem.find('.fe-paralax').css({
                        transform: 'translate(0%,' + (($(this).scrollTop()) - elem.offset().top) / 8 + '%)'
                    })
                })
            }
        };
    $.fn.feParalax = function (options) {
        return this.each(function () {
            var self = $(this),
                settings = $.extend({}, def, options);
                methods.init(self);
        });
    };
})(jQuery);